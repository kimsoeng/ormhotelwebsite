<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PaymentType;
use DB;

class PaymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['payment_type'] = DB::table('payment_type')->get();
        return view('admin.payment-type.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.payment-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $paymentType = PaymentType::create([
            'name' => $request->name
        ]);
        $i = $paymentType->save();
        if ($i) {
            return redirect()->route('paymentType.create')
                ->with('success', 'Data has been saved!');
        } else {
            return redirect()->route('paymentType.create')
                ->with('error', 'Fail to save data!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['payment_type'] = PaymentType::find($id);
        return view('admin/payment-type/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $data = array(
            'name' => $request->name,
        );
        $i = DB::table('payment_type')->where('id', $id)->update($data);
        if ($i) {
            return redirect()->route('paymentType.edit', $id)
                ->with('success', 'Data has been saved!');
        } else {
            return redirect()->route('paymentType.edit', $id)
                ->with('error', 'Fail to save data!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $i = PaymentType::where('id', $id)->delete();
        if ($i) {
            if ($i) {
                return redirect()->route('paymentType.index')
                    ->with('success', 'Data has been deleted!');
            } else {
                return redirect()->route('paymentType.index')
                    ->with('error', 'Fail to delete data!');
            }
        }
    }
}
