@extends('layouts.admin')
@section('style')
    <style>

    </style>
@endsection

@section('content')
    <section class="section">
        <div class="container">
            <h1>Create Branch</h1>
            <form action="{{route('branch.store')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="toolbox">
                    <button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
                        <i class="fa fa-save "></i> Save
                    </button>
                    <a href="{{route('branch.index')}}" class="btn btn-warning btn-oval btn-sm">
                        <i class="fa fa-reply"></i> Back</a>
                </div>
                <br>
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="card card-gray" style="padding: 57px">
                    <div class="card-block">
                        <div>
                            <div class="form-group row">
                                <label for="Name" class="col-sm-4 form-control-label">Branch Name <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="branchName" name="branchName"
                                           placeholder="Branch Name" value="{{old('branchName')}}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Type" class="col-sm-4 form-control-label">Description <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="type" name="shortDescription"
                                           placeholder="Short description" value="{{old('shortDescription')}}" required
                                           autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-sm-4 form-control-label">Image <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control-file" id="image" name="image"
                                           value="{{old('image')}}" required autofocus>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('js')
    <script>
        // Disable form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
@endsection
