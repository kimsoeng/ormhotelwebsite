@extends('layouts.admin')
@section('style')
<style>

</style>
@endsection

@section('content')
<section class="section">
  <div class="container">
    <h1>Edit User</h1>
    @foreach ($users as $c)
    <form action="/admin/update/<?php echo $users[0]->id; ?>" method="POST">
      <div class="toolbox">
        <button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
          <i class="fa fa-save "></i> Save</button>
        <a href="{{url('admin')}}" class="btn btn-warning btn-oval btn-sm">
          <i class="fa fa-reply"></i> Back</a>
      </div>
      <br>
      @if(session()->has('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}
      </div>
      @endif
      @if(session()->has('error'))
      <div class="alert alert-danger">
        {{ session()->get('error') }}
      </div>
      @endif
      <div class="card card-gray" style="padding: 57px">
        <div class="card-block">
          {{csrf_field()}}
          <div class="form-group row">
            <label for="Name" class="col-sm-4 form-control-label">Full Name <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Enter your full Name" value='<?php echo $users[0]->fullname; ?>' required autofocus>
            </div>
          </div>
          <div class="form-group row">
            <label for="Type" class="col-sm-4 form-control-label">Username <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name" value='<?php echo $users[0]->name; ?>' required autofocus>
            </div>
          </div>
          <div class="form-group row">
            <label for="Type" class="col-sm-4 form-control-label">Email <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="email" name="email" value='<?php echo $users[0]->email; ?>' required autofocus>
            </div>
          </div>
            <div class="form-group row">
              <label for="Type" class="col-sm-4 form-control-label">Password <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="password" class="form-control" id="password" name="password" value='<?php echo$users[0]->password; ?>' required autofocus>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    @endforeach
  </div>
</section>
@endsection