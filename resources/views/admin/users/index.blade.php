@extends('layouts.admin')
@section('style')
<style>
.users__btn__add {
   margin-bottom: 30px;
}

.users__table {
   width:100%;
}

.item__action {
   width: 140px;
}

.item__action__btn {
   display:flex; 
   justify-content:space-between;
}
</style>
@endsection

@section('content')            
<section class="section">
   <div class="users">
      <div class="row" style="margin-bottom: 20px;">
         <div class="col-md-6">
            <h1>Users</h1>
         </div>
         <div class="col-md-6" style="text-align: right;">
            <a href="{{url('admin/create')}}" class="btn btn-primary btn-sm" title="Create">
               <i class="fa fa-plus"></i> Create
            </a>
         </div>
      </div>
      <table id="example" class="table table-striped table-bordered users__table">
         <thead>
            <tr>
               <th>ID</th>
               <th>Full Name</th>
               <th>Username</th>
               <th>Email</th>
               <th>Action</th>
            </tr>
         </thead>
         <tbody>
                @php($i=1)
                @foreach ($users as $u)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$u->fullname}}</td>
                        <td>{{$u->name}}</td>
                        <td>{{$u->email}}</td>
                        <td>
                            <a href="{{url('admin/delete?id='.$u->id)}}" class="text-danger" title="Delete"
                            onclick="return confirm('Are you sure to delete?')">
                             
                                <i class="fa fa-trash"></i>
                            </a>
                            &nbsp;&nbsp;
                            <a  href='admin/edit/{{ $u->id }}' class="text-success">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
      </table>
   </div>
</section>
@stop