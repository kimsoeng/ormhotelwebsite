<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Hotel | Login</title>
  <!-- CSRF Token -->
  <title>{{ config('app.name', 'Hotel') }}</title>
  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
  <!-- Styles -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    body {background: #1cc3b2;}
    .btn-cancel {color: white;}
    .btn-cancel:hover {
      color: white;
      text-decoration: none
    }

    .invalid-feedback {
      font-size: 11px;
      color: red;
    }

    .text-white {
      color: white
    }

    .justify-content-center {
      margin-top: 100px;
    }

    .con-form {
      background: white;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
  </style>
</head>

<body>
  <div class="container" id="main-container">
    <div class="row justify-content-center">
      @yield('content')
    </div>
  </div>
</body>

</html>