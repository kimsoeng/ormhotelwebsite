@extends('layouts.front')
@section('style')
<style>
  .banner-section {
    background-image: url('{{ url("storage/images/banner_contact.jpg") }}');
    background-repeat: no-repeat;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 400px;
  }
  .con-career{
    border-radius: 5px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    padding: 60px;
  }
</style>
@endsection
@section('banner')
<div class="container">
  <div class="row" style="margin-top: 150px;">
    <h1 class="text-white" style="font-size:109px"><strong> Careers</strong></h1>
    <p class="text-white" style="font-size: 40px;"><strong>Join Our Team</strong></p>
  </div>
</div>
@endsection
@section('content')
<br><br>
<div class="container">
  <div class="row">
    <h1 class="text-center" style="font-size: 40px;"><strong>Explore opportunities to take your <br> career to the next level.</strong></h1>
    <p class="text-gray text-center">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dignissimos neque  dolorum est, minus pariatur <br> dolore recusandae iusto temporibu
      s quas, assumenda, culpa aliquid error ab quasi incidunt fugit laudantium laborum. Aliquam!</p>
  </div>
  <br><br>
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <img src="{{ url("storage/images/vision.jpg") }}" alt="" width="100%">
        <div class="card-body">
          <h3><strong> Working With Us</strong></h3>
          <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dignissimos neque  dolorum est,</p>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel">
        <img src="{{ url("storage/images/vision.jpg") }}" alt="" width="100%">
        <div class="panel-body">
          <h3><strong>Our Program</strong></h3>
          <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dignissimos neque  dolorum est,</p>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel">
        <img src="{{ url("storage/images/vision.jpg") }}" alt="" width="100%">
        <div class="panel-body">
          <h3><strong>People Stories</strong></h3>
          <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dignissimos neque  dolorum est,</p>
        </div>
      </div>
    </div>
  </div>
  <br><br><br>
  <div class="row">
    <h1 class="text-center" style="font-size: 40px;"><strong>Urgent Hiring</strong></h1>
    <p class="text-gray text-center">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dignissimos neque  dolorum est, minus pariatur <br> dolore recusandae iusto temporibu
      s quas, assumenda, culpa aliquid error ab quasi incidunt fugit laudantium laborum. Aliquam!</p>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10 con-career" id="con-career">
      <div class="row">
        <div class="col-md-4"><h4> <strong> Position</strong></h4></div>
        <div class="col-md-4"><h4> <strong> Location</strong></h4></div>
        <div class="col-md-4"><h4> <strong> Working Time</strong></h4></div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4"><p><a href="{{ url('career-detail') }}"> Designer</a></p></div>
        <div class="col-md-4"><p>Phnom Penh</p></div>
        <div class="col-md-4"><p>Full Time</p></div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-4"><p><a href="{{ url('career-detail') }}">Web Developer</a></p></div>
        <div class="col-md-4"><p>Phnom Penh</p></div>
        <div class="col-md-4"><p>Full Time</p></div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-4"><p><a href="{{ url('career-detail') }}">Backend developer</a></p></div>
        <div class="col-md-4"><p>Phnom Penh</p></div>
        <div class="col-md-4"><p>Full Time</p></div>
      </div><hr>
      <div class="row">
        <div class="col-md-4"><p><a href="{{ url('career-detail') }}">Data Entry</a></p></div>
        <div class="col-md-4"><p>Siem Reap</p></div>
        <div class="col-md-4"><p>Part Time</p></div>
      </div>
      </div>
    <div class="col-md-1"></div>
  </div>
</div>
@endsection