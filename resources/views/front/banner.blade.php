  <div class="col-md-12" style="margin-top: 331px;">
    <form>
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-2 form-group search-col">
          <label for="location" class="text-label">Where are you going?</label>
          <select class="form-control custom-input" id="location">
            <option>Phnom Penh</option>
            <option selected>Siem Reap</option>
            <option>Sihaknouk Ville</option>
          </select>
        </div>
        <div class="col-md-2 form-group search-col">
          <label for="checkInDate" class="text-label">Check In Date: </label>
          <input type="date" class="form-control custom-input" id="checkInDate" value="2020-08-17">
        </div>
        <div class="col-md-2 form-group search-col">
          <label for="checkOutDate" class="text-label">Check Out Date: </label>
          <input type="date" class="form-control custom-input" id="checkOutDate" value="2020-08-19">
        </div>
        <div class="col-md-2 form-group search-col">
          <label for="adult" class="text-label">Adult: </label>
          <select class="form-control custom-input" id="adult">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
          </select>
        </div>
        <div class="col-md-2 form-group search-col">
          <label for="children" class="text-label">Children: </label>
          <select class="form-control custom-input" id="children">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
          </select>
        </div>
        <div class="col-md-1 form-group search-col">
          <button type="button" class="btn btn-info btn-search"><i class="fa fa-search text-white"></i> Search</button>
        </div>
      </div>
    </form>
  </div>